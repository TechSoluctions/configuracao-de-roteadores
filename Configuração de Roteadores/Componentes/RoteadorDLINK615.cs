﻿using Configuração_de_Roteadores.Interfaces;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Configuração_de_Roteadores.Componentes
{
    public class RoteadorDLINK615 : RoteadorDLINK610
    {
        private String TAG = "Roteador DLINK 615";
        private IWebDriver driver;
        private String IP = "http://192.168.0.1";
        private String MAC = "";
        private String USUARIO = "hmg01";
        private String SENHA = "26145";
        private String PIN = "";
        private String PORTA = "32";
        private String VERSAO_HARDWARE = "";
        private String VERSAO_FIRMWARE = "";
        private bool ATUALIZACAO_COMPLETA = false;

        public RoteadorDLINK615(IWebDriver driver)
            : base(driver)
        {
            this.driver = driver;
            this.driver.Navigate().GoToUrl(IP);
        }

        public void Login()
        {
            this.driver.Navigate().GoToUrl(IP + "/login.htm");

            try
            {
                this.driver.FindElement(By.Id("username")).Clear();
                this.driver.FindElement(By.Id("username")).SendKeys("Admin");
                this.driver.FindElement(By.Id("password")).Clear();

                this.driver.FindElement(By.Id("loginBtn")).Click();
            }
            catch (OpenQA.Selenium.NoSuchElementException ex)
            {
                MessageBox.Show(ex.Message, TAG + " - Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (OpenQA.Selenium.NoSuchWindowException e)
            {
                MessageBox.Show(e.Message, TAG + " - Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Thread.Sleep(2000);
            try
            {
                var alert = this.driver.SwitchTo().Alert();
                alert.Accept();
            }
            catch (Exception) { }
        }
    }
}
