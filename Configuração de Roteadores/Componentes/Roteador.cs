﻿using Configuração_de_Roteadores.Interfaces;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Configuração_de_Roteadores.Componentes
{
    public class Roteador
    {
        public string TestePing(string IP)
        {
            //IPAddress.Parse(this.IP)
            var strCommandParameters = "/c ping " + IP + " -l 3200 -n 100";
            //System.Diagnostics.Process.Start("CMD.exe", strCmdText);

            //Create process
            System.Diagnostics.Process pProcess = new System.Diagnostics.Process();

            //strCommand is path and file name of command to run
            pProcess.StartInfo.FileName = "cmd.exe";

            //strCommandParameters are parameters to pass to program
            pProcess.StartInfo.Arguments = strCommandParameters;

            pProcess.StartInfo.UseShellExecute = false;

            //Set output of program to be written to process output stream
            pProcess.StartInfo.RedirectStandardOutput = true;
            //pProcess.StartInfo.StandardOutputEncoding = System.Text.Encoding.UTF8;

            //Optional
            //pProcess.StartInfo.WorkingDirectory = strWorkingDirectory;
            pProcess.StartInfo.CreateNoWindow = true;
            //Start the process
            pProcess.Start();

            //Get program output
            string strOutput = pProcess.StandardOutput.ReadToEnd();

            //Wait for process to finish
            pProcess.WaitForExit();

            //Console.WriteLine(strOutput);
            return this.PegarTextoPing(strOutput);
        }


        private string PegarTextoPing(string texto)
        {
            var textoSaida = "";
            var pegarTexto = false;
            var split = new char[] { Convert.ToChar("\r") };
            foreach (string linha in texto.Split(split))
            {
                var linhaSaida = linha.Trim();
                if (linhaSaida.Contains("Ping") || linhaSaida.Contains("Aproximar"))
                    pegarTexto = true;

                if (pegarTexto)
                    textoSaida += this.RemoveDiacritcs(linhaSaida) + "\r\n";
            }

            return textoSaida;
        }

        private string RemoveDiacritcs(string textoSaida)
        {            
            string[] ocorrencias = { "¡", "£", "M ximo", "M‚dia" };
            string[] solucao = { "í", "ú", "Máximo", "Média" };

            for (int posicao = 0; posicao < ocorrencias.Length; posicao++)
                textoSaida = textoSaida.Replace(ocorrencias[posicao], solucao[posicao]);

            return textoSaida;
        }
    }
}
