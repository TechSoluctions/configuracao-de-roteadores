﻿using Configuração_de_Roteadores.Interfaces;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Configuração_de_Roteadores.Componentes
{
    public class RoteadorDLINK610 : RoteadorInterface
    {
        private String TAG = "Roteador DLINK 610";
        private IWebDriver driver;
        private String IP = "http://192.168.0.1";
        private String MAC = "";
        private String USUARIO = "hmg01";
        private String SENHA = "26145";
        private String PIN = "";
        private String PORTA = "32";
        private String VERSAO_HARDWARE = "";
        private String VERSAO_FIRMWARE = "";
        private bool ATUALIZACAO_COMPLETA = false;

        public RoteadorDLINK610(IWebDriver driver)
        {            
            this.driver = driver;
            this.driver.Navigate().GoToUrl(IP);
        }

        public void Configurar()
        {
            this.ATUALIZACAO_COMPLETA = false;
            try
            {
                this.Login();
                Thread.Sleep(2000);
                this.getVersaoHardware();
                this.getVersaoFirmware();
                this.AtualizacaoFirmware();
                this.GetMACRoteador();
                this.Configuracao();
                this.Login();
                this.Wireless();
                this.Avancado();
                this.Manutencao();
                this.Close();

                MessageBox.Show("Roteador configurado com sucesso. \nIniciando teste de Ping.", TAG + " - Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.TestePing();

                this.ATUALIZACAO_COMPLETA = true;
            }
            catch (Exception e)
            {
                this.ATUALIZACAO_COMPLETA = false;
            }
        }

        public void TestePing()
        {
            var roteador = new Roteador();
            var retorno = roteador.TestePing(this.IP);
            MessageBox.Show(retorno, TAG + " - Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public void Login()
        {
            this.driver.Navigate().GoToUrl(IP + "/login.htm");

            try
            {
                this.driver.FindElement(By.Id("username")).Clear();
                this.driver.FindElement(By.Id("username")).SendKeys("Admin");
                this.driver.FindElement(By.Id("password")).Clear();

                this.driver.FindElement(By.Id("loginBtn")).Click();                
            }
            catch (OpenQA.Selenium.NoSuchElementException ex)
            {
                MessageBox.Show(ex.Message, TAG + " - Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (OpenQA.Selenium.NoSuchWindowException e)
            {
                MessageBox.Show(e.Message, TAG + " - Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void GetMACRoteador()
        {
            try
            {
                this.driver.SwitchTo().Frame(1);
            }
            catch (OpenQA.Selenium.NoSuchFrameException) { }

            try
            {
                var maincontent = this.driver.FindElement(By.Id("maincontent"));
                var ConfiguracaoDeLAN = maincontent.FindElement(By.XPath("table[3]/tbody/tr[2]/td"));
                var mac = ConfiguracaoDeLAN.FindElement(By.XPath("table/tbody/tr[4]/td[2]"));
                this.MAC = mac.Text;
            }
            catch (OpenQA.Selenium.NoSuchElementException e)
            {
                MessageBox.Show(e.Message, TAG + " - Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }            
        }


        public void Configuracao()
        {
            var topnav_container2 = this.driver.FindElement(By.Id("topnav_container2"));
            var button = topnav_container2.FindElement(By.XPath("tbody/tr/td[4]/a"));
            button.Click();
            Thread.Sleep(2000);

            var maincontent = this.driver.FindElement(By.Id("maincontent"));
            var wizardstep1_back = this.driver.FindElement(By.Name("wizardstep1_back"));
            wizardstep1_back.Click();

            Thread.Sleep(2000);
            var wantype = this.driver.FindElement(By.Name("wantype"));
            var TipoDeAcessoWAN = new SelectElement(wantype);
            var pppoeSelected = false;
            foreach (var option in TipoDeAcessoWAN.Options)
            {                
                var optionText = option.Text.Trim().ToLower();
                if (optionText.Equals("pppoe"))
                {
                    option.Click();
                    pppoeSelected = true;
                }                    
            }            

            if (pppoeSelected)
            {
                var pppoe_usrname = this.driver.FindElement(By.Name("pppoe_usrname"));
                var pppoe_psword = this.driver.FindElement(By.Name("pppoe_psword"));
                var pppoe_sername = this.driver.FindElement(By.Name("pppoe_sername"));
                var pppoe_mtusize = this.driver.FindElement(By.Name("pppoe_mtusize"));

                pppoe_usrname.Clear();
                pppoe_psword.Clear();
                pppoe_sername.Clear();
                pppoe_mtusize.Clear();

                pppoe_usrname.SendKeys(this.USUARIO);
                pppoe_psword.SendKeys(this.SENHA);
                pppoe_sername.SendKeys("CENSANET");
                pppoe_mtusize.SendKeys("1452");


                var pppoe_contype = this.driver.FindElement(By.Name("pppoe_contype"));
                var TipoDeConexao = new SelectElement(pppoe_contype);
                TipoDeConexao.Options[0].Click();

                var dns_ctrl = this.driver.FindElement(By.Name("dns_ctrl"));
                dns_ctrl.Click(); // rever
            }            
            
            Thread.Sleep(2000);
            var save = this.driver.FindElement(By.Name("save"));
            save.Click();

            try
            {
                Thread.Sleep(2000);
                var alert = this.driver.SwitchTo().Alert();
                alert.Accept();
            }
            catch (NoAlertPresentException) { }
        }

        public void Wireless()
        {
            try
            {
                this.driver.SwitchTo().Frame(1);
            }
            catch (OpenQA.Selenium.NoSuchFrameException) { }

            try
            {
                WebDriverWait wait = new WebDriverWait(this.driver, TimeSpan.FromSeconds(70));
                var topnav_container2 = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("topnav_container2")));
                var button = topnav_container2.FindElement(By.XPath("tbody/tr/td[6]/a"));
                button.Click();
                Thread.Sleep(2000);

                var sidenav = this.driver.FindElement(By.Id("sidenav"));
                var avancado = sidenav.FindElement(By.XPath("ul/li[3]/div/a"));
                avancado.Click();
                Thread.Sleep(2000);

                var content = this.driver.FindElement(By.Name("advanceSetup"));            
                var pin = content.FindElement(By.XPath("table[3]/tbody/tr[2]/td/table/tbody/tr[1]/td[2]")).Text;
                this.PIN = pin.Trim();            

                sidenav = this.driver.FindElement(By.Id("sidenav"));
                var admin = sidenav.FindElement(By.XPath("ul/li[1]/div/a"));
                admin.Click();
                Thread.Sleep(2000);

                var mac = this.MAC.Split(new char[] { Convert.ToChar(":") });
                var tamanho = mac.Length;
            
                var ssid = this.driver.FindElement(By.Name("ssid"));
                ssid.Clear();
                ssid.SendKeys("CENSANETWIFI-" + mac[tamanho - 2] + mac[tamanho - 1]);

                var chan = this.driver.FindElement(By.Name("chan"));
                var Canal = new SelectElement(chan);
                Canal.Options[0].Click();

                if (!String.IsNullOrWhiteSpace(this.PIN))
                {
                    var method = this.driver.FindElement(By.Name("method"));
                    var OpcoesDeSegurança = new SelectElement(method);
                    OpcoesDeSegurança.Options[4].Click();


                    var pskValue = this.driver.FindElement(By.Name("pskValue"));
                    pskValue.Clear();
                    pskValue.SendKeys(this.PIN);
                }            

                Thread.Sleep(2000);
                var save = this.driver.FindElement(By.Name("save"));
                save.Click();
            }
            catch (OpenQA.Selenium.NoSuchElementException e) 
            {
                MessageBox.Show(e.Message, TAG + " - Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Avancado()
        {
            WebDriverWait wait = new WebDriverWait(this.driver, TimeSpan.FromSeconds(70));
            var topnav_container2 = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("topnav_container2")));
            var button = topnav_container2.FindElement(By.XPath("tbody/tr/td[8]/a"));
            button.Click();
            Thread.Sleep(2000);

            var wansetting = this.driver.FindElement(By.Name("wansetting"));
            var ConfiguracaoWAN = new SelectElement(wansetting);
            ConfiguracaoWAN.Options[0].Click();

            var localAcl_web = this.driver.FindElement(By.Name("localAcl_web"));
            localAcl_web.Click();

            var localAcl_ping = this.driver.FindElement(By.Name("localAcl_ping"));
            localAcl_ping.Click();

            var webport = this.driver.FindElement(By.Name("webport"));
            webport.Clear();
            webport.SendKeys(this.PORTA);

            var formFilterEdit = this.driver.FindElement(By.Name("formFilterEdit"));
            formFilterEdit.Submit();
        }

        public void Manutencao()
        {
            var mac = this.MAC.Split(new char[] { Convert.ToChar(":") });
            var tamanho = mac.Length;
            var senha = mac[tamanho - 3] + mac[tamanho - 2] + mac[tamanho - 1];

            WebDriverWait wait = new WebDriverWait(this.driver, TimeSpan.FromSeconds(70));
            var topnav_container2 = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("topnav_container2")));
            var button = topnav_container2.FindElement(By.XPath("tbody/tr/td[10]/a"));
            button.Click();
            Thread.Sleep(2000);

            var sidenav = this.driver.FindElement(By.Id("sidenav"));
            var admin = sidenav.FindElement(By.XPath("ul/li[4]/div/a"));
            admin.Click();
            Thread.Sleep(2000);

            try
            {
                var select = this.driver.FindElement(By.Name("select"));
                select.Click();

                var username = this.driver.FindElement(By.Name("username"));
                username.Clear();
                username.SendKeys("Admin");

                var newpass = this.driver.FindElement(By.Name("newpass"));
                newpass.Clear();
                newpass.SendKeys(senha);

                var confpass = this.driver.FindElement(By.Name("confpass"));
                confpass.Clear();
                confpass.SendKeys(senha);
            }
            catch (OpenQA.Selenium.InvalidElementStateException e)
            {
                MessageBox.Show(e.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }            
            /*
            var adduser = this.driver.FindElement(By.Name("adduser"));
            adduser.Click();*/

            var modify = this.driver.FindElement(By.Name("modify"));
            modify.Click();            
        }

        public void getVersaoHardware()
        {
            try
            {
                try
                {
                    this.driver.SwitchTo().Frame(1);
                }
                catch (OpenQA.Selenium.NoSuchFrameException) { }

                try
                {
                    var header_container = this.driver.FindElement(By.Id("header_container"));
                    var textoVersao = header_container.FindElement(By.XPath("tbody/tr[1]/td")).Text;

                    var split = new char[] { Convert.ToChar(":"), Convert.ToChar(" ") };
                    var versoes = textoVersao.Split(split);

                    this.VERSAO_HARDWARE = versoes[3].Trim();
                }
                catch (OpenQA.Selenium.NoSuchElementException) { }
            }
            catch (OpenQA.Selenium.UnhandledAlertException ex)
            {
                this.Close();
                MessageBox.Show("Roteador já se encontra configurado, para prosseguir RESET o roteador.", TAG + " - Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public void getVersaoFirmware()
        {
            try{
                try 
                {
                    this.driver.SwitchTo().Frame(1);
                }
                catch (OpenQA.Selenium.NoSuchFrameException) { }

                try
                {
                    var header_container = this.driver.FindElement(By.Id("header_container"));
                    var textoVersao = header_container.FindElement(By.XPath("tbody/tr[1]/td")).Text;
                    var versoes = textoVersao.Split(new char[] { Convert.ToChar(":") });

                    this.VERSAO_FIRMWARE = versoes[versoes.Length - 1].Trim();
                }
                catch (OpenQA.Selenium.NoSuchElementException) { }       
            }
            catch (OpenQA.Selenium.UnhandledAlertException ex) {
                this.Close();
                MessageBox.Show("Roteador já se encontra configurado, para prosseguir RESET o roteador.", TAG + " - Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public void AtualizacaoFirmware()
        {
            if (String.IsNullOrWhiteSpace(this.VERSAO_HARDWARE))
                this.getVersaoHardware();

            if (String.IsNullOrWhiteSpace(this.VERSAO_FIRMWARE))
                this.getVersaoFirmware();

            var currentPath = System.IO.Path.GetDirectoryName(Application.ExecutablePath);
            string[] versoes = Directory.GetFiles(currentPath + "\\Atualizadores", "*.img")
                                     .Select(Path.GetFileName)
                                     .ToArray();

            var arquivoAtualizacao = "";
            foreach (String versao in versoes)
            {
                var split = new Char[] { Convert.ToChar("_") };
                var versaoSplit = versao.Split(split);

                if (versaoSplit[0].Equals(this.TAG))
                {
                    var hardware = versaoSplit[1].Trim();
                    var firmware = versaoSplit[2].Trim().Replace(" ", ".").Replace(".img", "");

                    if (hardware.Equals(this.VERSAO_HARDWARE))
                    {
                        var currentFirmwareDouble = Convert.ToDouble(this.VERSAO_FIRMWARE);
                        var firmwareDouble = Convert.ToDouble(firmware);

                        if (firmwareDouble > currentFirmwareDouble)                        
                            arquivoAtualizacao = versao;                        
                    }
                }
            }

            if (!String.IsNullOrWhiteSpace(arquivoAtualizacao))
            {
                var caminhoArquivoAtualizacao = currentPath + "\\Atualizadores\\" + arquivoAtualizacao;

                WebDriverWait wait = new WebDriverWait(this.driver, TimeSpan.FromSeconds(70));
                var topnav_container2 = wait.Until(ExpectedConditions.ElementIsVisible(By.Id("topnav_container2")));
                var button = topnav_container2.FindElement(By.XPath("tbody/tr/td[10]/a"));
                button.Click();
                Thread.Sleep(2000);

                var sidenav = this.driver.FindElement(By.Id("sidenav"));
                var admin = sidenav.FindElement(By.XPath("ul/li[2]/div/a"));
                admin.Click();
                Thread.Sleep(2000);

                var body_header = this.driver.FindElement(By.Id("body_header"));
                var arquivo = body_header.FindElement(By.XPath("tbody/tr[2]/td/input"));
                arquivo.Clear();
                arquivo.SendKeys(caminhoArquivoAtualizacao);

                var restaura = body_header.FindElement(By.XPath("tbody/tr[3]/td/input"));
                if (!restaura.Selected)
                    restaura.Click();

                var formAtualizacao = this.driver.FindElement(By.Name("password"));
                formAtualizacao.Submit();

                Thread.Sleep(73000);
                this.Configurar(); 
            }
        }

        public void Close()
        {
            this.driver.Close();
        }         

        public string GetPIN()
        {
            return this.PIN;
        }

        public string GetRoteadorGLPI()
        {
            return "roteador_610_n150mb";
        }

        public string GetMAC()
        {
            return this.MAC.Replace(":", "");
        }

        public bool Atualizado()
        {
            return this.ATUALIZACAO_COMPLETA;
        }

        public void setModeloRoteador(string modelo)
        {
            throw new NotImplementedException();
        }

        public void setModeloRoteador(int modelo)
        {
            throw new NotImplementedException();
        }

        public void IniciarModelosRoteador()
        {
            throw new NotImplementedException();
        }
    }
}
