﻿using Configuração_de_Roteadores.Interfaces;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Configuração_de_Roteadores.Componentes
{
    public class RoteadorMultilaser : RoteadorInterface
    {
        private String TAG = "Roteador Multilaser ";
        private IWebDriver driver;
        private String IP = "http://10.0.0.2";
        private String MAC = "";
        private String USUARIO = "hmg01";
        private String SENHA = "26145";
        private String PIN = "";
        private String PORTA = "32";
        private String VERSAO_HARDWARE = "";
        private String VERSAO_FIRMWARE = "";
        private Dictionary<int, String> modelosRoteador;
        private int MODELO_ROTEADOR = 0;
        private bool ATUALIZACAO_COMPLETA = false;


        public RoteadorMultilaser(IWebDriver driver)
        {            
            this.driver = driver;
            this.driver.Navigate().GoToUrl(IP);
            this.IniciarModelosRoteador();
        }

        public void Configurar()
        {
            this.ATUALIZACAO_COMPLETA = false;
            try
            {
                this.Login();
                Thread.Sleep(2000);
                /*this.getVersaoHardware();
                this.getVersaoFirmware();
                this.AtualizacaoFirmware();*/
                this.GetMACRoteador();
                Thread.Sleep(2000);
                this.Configuracao();
                Thread.Sleep(2000);
                this.Wireless();
                Thread.Sleep(2000);
                this.Avancado();
                Thread.Sleep(2000);
                this.Manutencao(); 
                this.Close();

                MessageBox.Show("Roteador configurado com sucesso. \nIniciando teste de Ping.", TAG + " - Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.TestePing();

                this.ATUALIZACAO_COMPLETA = true;
            }
            catch (Exception e)            
            {
                MessageBox.Show(e.Message, TAG + " - Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.ATUALIZACAO_COMPLETA = false;
            }
        }

        public void Login()
        {            
            this.driver.Navigate().GoToUrl("http://admin:admin@" + this.IP.Replace("http://", ""));
            Thread.Sleep(2000);
            this.driver.Navigate().GoToUrl(IP + "/wizard.htm"); 

            try
            {
                var form = this.driver.FindElement(By.Name("wizard"));
                var button = form.FindElement(By.XPath("table/tbody/tr[2]/td[1]/table/tbody/tr/td[3]/img"));
                button.Click();                
            }
            catch (OpenQA.Selenium.NoSuchElementException ex)
            {
                MessageBox.Show(ex.Message, TAG + " - Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (OpenQA.Selenium.NoSuchWindowException e)
            {
                MessageBox.Show(e.Message, TAG + " - Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void getVersaoHardware()
        {
            throw new NotImplementedException();
        }

        public void getVersaoFirmware()
        {
            throw new NotImplementedException();
        }

        public void AtualizacaoFirmware()
        {
            throw new NotImplementedException();
        }

        public void GetMACRoteador()
        {
            try
            {
                this.EscolherFrame(3);
                var Gerenciamento = this.driver.FindElement(By.Id("Instalação"));
                Gerenciamento.Click();

                this.EscolherFrame(4);
                var fr = this.driver;
                var contentMain = this.driver.FindElements(By.ClassName("tbl_head"));
                var contentMac = contentMain[2];
                var mac = contentMac.FindElement(By.XPath("../tr[19]/td[2]"));
                this.MAC = mac.Text;
            }
            catch (OpenQA.Selenium.NoSuchElementException ex)
            {
                MessageBox.Show(ex.Message, TAG + " - Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (OpenQA.Selenium.NoSuchWindowException e)
            {
                MessageBox.Show(e.Message, TAG + " - Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Configuracao()
        {
            try
            {
                this.EscolherFrame(3);
                var tcpIp = this.driver.FindElement(By.Id("TCP/IP"));
                tcpIp.Click();

                // Configuração de WAN
                this.EscolherFrame(2);
                var configuracaoWan = this.driver.FindElement(By.Id("Configuração de WAN"));
                configuracaoWan.Click();

                this.EscolherFrame(4);

                var wanType_id = this.driver.FindElement(By.Id("wanType_id"));
                var TipoAcesso = new SelectElement(wanType_id);
                TipoAcesso.Options[2].Click();

                var pppUserName = this.driver.FindElement(By.Name("pppUserName"));
                var pppPassword = this.driver.FindElement(By.Name("pppPassword"));
                var pppServiceName = this.driver.FindElement(By.Name("pppServiceName"));
                var pppMtuSize = this.driver.FindElement(By.Name("pppMtuSize"));

                pppUserName.Clear();
                pppPassword.Clear();
                pppServiceName.Clear();
                pppMtuSize.Clear();

                pppUserName.SendKeys(this.USUARIO);
                pppPassword.SendKeys(this.SENHA);
                pppServiceName.SendKeys("CENSANET");
                pppMtuSize.SendKeys("1452");

                var pppConnectType = this.driver.FindElement(By.Name("pppConnectType"));
                var TipoConexao = new SelectElement(pppConnectType);
                TipoConexao.Options[0].Click();

                this.driver.FindElement(By.Name("upnpEnabled")).Click();
                this.driver.FindElement(By.Name("webWanAccess")).Click();

                var formTcpip = this.driver.FindElement(By.Name("tcpip"));
                formTcpip.Submit();
            }
            catch (OpenQA.Selenium.NoSuchElementException ex)
            {
                MessageBox.Show(ex.Message, TAG + " - Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (OpenQA.Selenium.NoSuchWindowException e)
            {
                MessageBox.Show(e.Message, TAG + " - Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }  
        }

        public void Wireless()
        {            
            try
            {
                this.EscolherFrame(3);
                var wlan1 = this.driver.FindElement(By.Id("wlan1"));
                wlan1.Click();

                // Configuração básica
                this.EscolherFrame(2);
                var configuracaoBasica = this.driver.FindElement(By.Id("Configuração básica"));
                configuracaoBasica.Click();

                var mac = this.MAC.Split(new char[] { Convert.ToChar(":") });
                var tamanho = mac.Length;

                this.EscolherFrame(4);

                var ssid0 = this.driver.FindElement(By.Name("ssid0"));
                ssid0.Clear();
                ssid0.SendKeys("CENSANETWIFI-" + mac[tamanho - 2] + mac[tamanho - 1]);

                var chan = this.driver.FindElement(By.Name("chan0"));
                var Canal = new SelectElement(chan);
                Canal.Options[0].Click();

                var formConfiguracaoBasica = this.driver.FindElement(By.Name("wlanSetup"));
                formConfiguracaoBasica.Submit();

                // Avançado
                this.EscolherFrame(2);
                var Avancado = this.driver.FindElement(By.Id("Avançado"));
                Avancado.Click();

                this.EscolherFrame(4);
                var coexist_ = this.driver.FindElements(By.Name("coexist_"));
                coexist_[0].Click();

                var formAvancado = this.driver.FindElement(By.Name("advanceSetup"));
                formAvancado.Submit();
                
                // PIN 
                this.EscolherFrame(2);
                var WPS = this.driver.FindElement(By.Id("WPS"));
                WPS.Click();

                this.EscolherFrame(4);
                var root_hide_div = this.driver.FindElement(By.Id("root_hide_div"));
                var pin = root_hide_div.FindElement(By.XPath("table[1]/tbody/tr[5]/td[2]"));
                this.PIN = pin.Text;

                var chaveDeSeguranca = root_hide_div.FindElement(By.XPath("table[5]/tbody/tr[2]/td[3]")).Text;

                // Segurança
                /*
                this.EscolherFrame(2);
                var seguranca = this.driver.FindElement(By.Id("Segurança"));
                seguranca.Click();
                 
                this.EscolherFrame(4);
                var frame = this.driver.FindElement(By.Id("SSIDAuthMode"));
                var n = frame.FindElement(By.Name("formEncrypt"));

                var wpapsk = n.FindElement(By.Id("wpapsk"));
                wpapsk.Clear();
                wpapsk.SendKeys(chaveDeSeguranca);

                //this.EscolherFrame(4);
                var formEncrypt = this.driver.FindElement(By.Name("formEncrypt"));
                formEncrypt.Submit();*/
            }
            catch (OpenQA.Selenium.NoSuchElementException ex)
            {
                MessageBox.Show(ex.Message, TAG + " - Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (OpenQA.Selenium.NoSuchWindowException e)
            {
                MessageBox.Show(e.Message, TAG + " - Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }           
        }

        public void Avancado()
        {
            try 
            {
                this.EscolherFrame(3);
                var gerenciamento = this.driver.FindElement(By.Id("Gerenciamento de"));
                gerenciamento.Click();

                this.EscolherFrame(2);
                this.driver.FindElement(By.Id("Log ")).Click();

                this.EscolherFrame(4);
                var log = this.driver.FindElement(By.Name("logEnabled")); //.Click();
                if (!log.Selected)
                    log.Click();

                var syslogEnabled = this.driver.FindElement(By.Name("syslogEnabled")); //.Click();
                if (!syslogEnabled.Selected)
                    syslogEnabled.Click();

                this.driver.FindElement(By.Name("Apply")).Click();                 
            }
            catch (OpenQA.Selenium.NoSuchElementException ex)
            {
                MessageBox.Show(ex.Message, TAG + " - Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (OpenQA.Selenium.NoSuchWindowException e)
            {
                MessageBox.Show(e.Message, TAG + " - Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }         
        }

        public void Manutencao()
        {
            var mac = this.MAC.Split(new char[] { Convert.ToChar(":") });
            var tamanho = mac.Length;
            var senha = mac[tamanho - 3] + mac[tamanho - 2] + mac[tamanho - 1];

            try
            {
                this.EscolherFrame(3);
                var gerenciamento = this.driver.FindElement(By.Id("Gerenciamento de"));
                gerenciamento.Click();

                this.EscolherFrame(2);
                this.driver.FindElement(By.Id("Senha")).Click();

                this.EscolherFrame(4);
                var userName = this.driver.FindElement(By.Name("userName"));
                userName.Clear();
                userName.SendKeys("Admin");

                var newPass = this.driver.FindElement(By.Name("newPass"));
                newPass.Clear();
                newPass.SendKeys(senha);

                var confPass = this.driver.FindElement(By.Name("confPass"));
                confPass.Clear();
                confPass.SendKeys(senha);

                this.driver.FindElement(By.Name("password")).Submit();
            }
            catch (OpenQA.Selenium.NoSuchElementException ex)
            {
                MessageBox.Show(ex.Message, TAG + " - Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (OpenQA.Selenium.NoSuchWindowException e)
            {
                MessageBox.Show(e.Message, TAG + " - Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }         
        }

        public void Close()
        {
            this.driver.Close();
        }

        public string GetMAC()
        {
            return this.MAC;
        }

        public string GetPIN()
        {
            return this.PIN;
        }

        public string GetRoteadorGLPI()
        {
            throw new NotImplementedException();
        }

        public bool Atualizado()
        {
            //throw new NotImplementedException();

            return this.ATUALIZACAO_COMPLETA;
        }

        public void TestePing()
        {
            //throw new NotImplementedException();
        }


        public void setModeloRoteador(int modelo)
        {
            if (this.modelosRoteador == null || this.modelosRoteador.Count == 0)
                this.IniciarModelosRoteador();

            this.MODELO_ROTEADOR = modelo;
            this.TAG = "Roteador Multilaser " + this.modelosRoteador[this.MODELO_ROTEADOR];
        }


        public void IniciarModelosRoteador()
        {
            this.modelosRoteador = new Dictionary<int, string>();
            this.modelosRoteador.Add(1, "Wireless N 300Mps - IPV6");
        }

        protected bool EscolherFrame(int frame)
        {
            try
            {
                this.driver.SwitchTo().DefaultContent();
                this.driver.SwitchTo().Frame(frame);

                return true;
            }
            catch (OpenQA.Selenium.NoSuchFrameException) 
            {
                return false;
            }
        }
    }
}
