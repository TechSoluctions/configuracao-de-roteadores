﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Configuração_de_Roteadores.Interfaces
{
    public interface RoteadorInterface
    {
        void Configurar();              // Inicializa o passo a passo para a configuração do roteador
        void Login();                   // Faz o Login no roteador para acessar as configurações
        void getVersaoHardware();       // Pega a Versão do Hardware após o login efetuado
        void getVersaoFirmware();       // Pega a Versão do Firmware após o login efetuado
        void AtualizacaoFirmware();     // Atualiza o Firmware no Roteador baseado na versão atual com a passada pelo Usuário
        void GetMACRoteador();          // Pega o MAC do Roteador após o login efetuado
        void Configuracao();            // Acessa a Aba de Configuração no menu para fazer as configurações no roteador
        void Wireless();                // Acessa a Aba de Wireless no menu para fazer as configurações no roteador
        void Avancado();                // Acessa a Aba de Avançado no menu para fazer as configurações no roteador
        void Manutencao();              // Acessa a Aba de Manutenção/Maintenance no menu para fazer as configurações no roteador
        void Close();                   // Fecha o navegador
        string GetMAC();
        string GetPIN();
        string GetRoteadorGLPI();
        bool Atualizado();
        void TestePing();
        void setModeloRoteador(int modelo);
        void IniciarModelosRoteador();
    }
}
