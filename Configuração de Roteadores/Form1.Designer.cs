﻿namespace Configuração_de_Roteadores
{
    partial class FormConfiguracao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxTipoRoteador = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonConfigurarRoteador = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxNumeroSerie = new System.Windows.Forms.TextBox();
            this.panelContainerStatus = new System.Windows.Forms.Panel();
            this.progressBarStatusConfiguracao = new System.Windows.Forms.ProgressBar();
            this.labelStatusConfiguracao = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.panelContainerStatus.SuspendLayout();
            this.SuspendLayout();
            // 
            // comboBoxTipoRoteador
            // 
            this.comboBoxTipoRoteador.FormattingEnabled = true;
            this.comboBoxTipoRoteador.Items.AddRange(new object[] {
            "Roteador DIR-610 Wireless N 150Mbps",
            "Roteador DIR-615 Wireless N 300Mbps",
            "Roteador Multilaser Wireless N 300Mbps - IPV6"});
            this.comboBoxTipoRoteador.Location = new System.Drawing.Point(39, 57);
            this.comboBoxTipoRoteador.Name = "comboBoxTipoRoteador";
            this.comboBoxTipoRoteador.Size = new System.Drawing.Size(249, 21);
            this.comboBoxTipoRoteador.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(35, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(225, 23);
            this.label1.TabIndex = 1;
            this.label1.Text = "Roteadores Suportados:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonConfigurarRoteador);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textBoxNumeroSerie);
            this.groupBox1.Location = new System.Drawing.Point(39, 102);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(249, 80);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Configuração de Roteador";
            // 
            // buttonConfigurarRoteador
            // 
            this.buttonConfigurarRoteador.Location = new System.Drawing.Point(120, 45);
            this.buttonConfigurarRoteador.Name = "buttonConfigurarRoteador";
            this.buttonConfigurarRoteador.Size = new System.Drawing.Size(123, 23);
            this.buttonConfigurarRoteador.TabIndex = 3;
            this.buttonConfigurarRoteador.Text = "Configurar Roteador";
            this.buttonConfigurarRoteador.UseVisualStyleBackColor = true;
            this.buttonConfigurarRoteador.Click += new System.EventHandler(this.buttonConfigurarRoteador_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Nº de Série:";
            // 
            // textBoxNumeroSerie
            // 
            this.textBoxNumeroSerie.Location = new System.Drawing.Point(76, 19);
            this.textBoxNumeroSerie.Name = "textBoxNumeroSerie";
            this.textBoxNumeroSerie.Size = new System.Drawing.Size(167, 20);
            this.textBoxNumeroSerie.TabIndex = 3;
            // 
            // panelContainerStatus
            // 
            this.panelContainerStatus.Controls.Add(this.labelStatusConfiguracao);
            this.panelContainerStatus.Controls.Add(this.progressBarStatusConfiguracao);
            this.panelContainerStatus.Location = new System.Drawing.Point(-4, 188);
            this.panelContainerStatus.Name = "panelContainerStatus";
            this.panelContainerStatus.Size = new System.Drawing.Size(339, 36);
            this.panelContainerStatus.TabIndex = 3;
            this.panelContainerStatus.Visible = false;
            // 
            // progressBarStatusConfiguracao
            // 
            this.progressBarStatusConfiguracao.Location = new System.Drawing.Point(43, 2);
            this.progressBarStatusConfiguracao.Name = "progressBarStatusConfiguracao";
            this.progressBarStatusConfiguracao.Size = new System.Drawing.Size(249, 12);
            this.progressBarStatusConfiguracao.TabIndex = 4;
            // 
            // labelStatusConfiguracao
            // 
            this.labelStatusConfiguracao.AutoSize = true;
            this.labelStatusConfiguracao.Location = new System.Drawing.Point(40, 19);
            this.labelStatusConfiguracao.Name = "labelStatusConfiguracao";
            this.labelStatusConfiguracao.Size = new System.Drawing.Size(121, 13);
            this.labelStatusConfiguracao.TabIndex = 4;
            this.labelStatusConfiguracao.Text = "Configurando roteador...";
            // 
            // FormConfiguracao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(329, 336);
            this.Controls.Add(this.panelContainerStatus);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBoxTipoRoteador);
            this.Name = "FormConfiguracao";
            this.Text = "Configuração de Roteadores";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormConfiguracao_FormClosed);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panelContainerStatus.ResumeLayout(false);
            this.panelContainerStatus.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxTipoRoteador;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button buttonConfigurarRoteador;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxNumeroSerie;
        private System.Windows.Forms.Panel panelContainerStatus;
        private System.Windows.Forms.Label labelStatusConfiguracao;
        private System.Windows.Forms.ProgressBar progressBarStatusConfiguracao;
    }
}

