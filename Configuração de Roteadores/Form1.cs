﻿using Configuração_de_Roteadores.Componentes;
using Configuração_de_Roteadores.Interfaces;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Configuração_de_Roteadores
{
    public partial class FormConfiguracao : Form
    {
        private Thread mThread;
        private Thread mThreadStatus;
        private IWebDriver driver;
        private GLPI glpi;

        public FormConfiguracao()
        {
            InitializeComponent();
            glpi = new GLPI();
        }

        private void buttonConfigurarRoteador_Click(object sender, EventArgs e)
        {
            var currentPath = System.IO.Path.GetDirectoryName(Application.ExecutablePath);
            var NumeroDeSerie = this.textBoxNumeroSerie.Text;

            if (!String.IsNullOrWhiteSpace(NumeroDeSerie))
            {
                var chromeOptions = new ChromeOptions
                {
                    BinaryLocation = @"C:\Users\jrodolfo\Documents\Visual Studio 2013\Projects\Configuração de Roteadores\Configuração de Roteadores\bin\Debug\chromedriver.exe",
                };

                var options = new ChromeOptions();
                options.AddArguments("test-type");
                options.ToCapabilities();
                ChromeDriverService service = ChromeDriverService.CreateDefaultService(currentPath);
                service.HideCommandPromptWindow = true;

                RoteadorInterface mRoteadorInterface = null;
                var tipo = this.getTipoRoteadorSelecionado();

                if (tipo != -1)
                {
                    this.driver = new ChromeDriver(service, options);

                    switch (tipo)
                    {
                        case 0:
                            mRoteadorInterface = new RoteadorDLINK610(this.driver);
                            break;

                        case 1:
                            mRoteadorInterface = new RoteadorDLINK615(this.driver);
                            break;

                        case 2:
                            mRoteadorInterface = new RoteadorMultilaser(this.driver);
                            mRoteadorInterface.setModeloRoteador(1);
                            break;
                    }

                    try
                    {
                        mRoteadorInterface.Configurar();
                        if (mRoteadorInterface.Atualizado())
                            glpi.Escrever(new String[] { mRoteadorInterface.GetMAC(), mRoteadorInterface.GetPIN(), NumeroDeSerie, mRoteadorInterface.GetRoteadorGLPI() });

                        /*
                        //this.mThread = new Thread(() => this.IncreaseProgressBarStatus(70, 500, "Configurando roteador..."));
                        //mThread.Start();
                        this.mThread = new Thread(delegate()
                        {
                            mRoteadorInterface.Configurar();
                            //var configurou = mRoteadorInterface.Atualizado();
                            //if (configurou)
                            //{
                                mThreadStatus = new Thread(() => this.IncreaseProgressBarStatus(100, 500, "Testando Ping"));
                                mThreadStatus.Start();

                                mRoteadorInterface.TestePing();
                                glpi.Escrever(new String[] { mRoteadorInterface.GetMAC(), mRoteadorInterface.GetPIN(), NumeroDeSerie, mRoteadorInterface.GetRoteadorGLPI() });
                            //}
                        });

                        mThread.Start();

                        //this.IncreaseProgressBarStatus(70, 500, "Configurando roteador...");
                        this.mThreadStatus = new Thread(() => this.IncreaseProgressBarStatus(70, 500, "Configurando roteador..."));
                        mThreadStatus.Start();
                         * */
                    }
                    catch (NullReferenceException ex)
                    {
                        MessageBox.Show(ex.Message, "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Nenhum Roteador informado para a Configuração.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Por favor, informe o Número de Série para iniciar a Configuração do " + this.comboBoxTipoRoteador.Text + ".", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }            
        }

        private void FormConfiguracao_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (this.driver != null)
            {
                try
                {
                    this.driver.Close();
                    this.mThread.Abort();
                }
                catch (System.InvalidOperationException) { }
                catch (System.NullReferenceException) { }
            }                
        }

        protected int getTipoRoteadorSelecionado()
        {
            return this.comboBoxTipoRoteador.SelectedIndex;
        }

        protected void IncreaseProgressBarStatus(int progress, int timeSleep, string messageStatus)
        {
            if (!this.panelContainerStatus.Visible && progress > 0)
                this.BeginInvoke((MethodInvoker)delegate() { this.panelContainerStatus.Visible = true; });

            //if (!String.IsNullOrWhiteSpace(messageStatus))
            //this.labelStatusConfiguracao.Text = messageStatus;
            
            this.BeginInvoke((MethodInvoker)delegate() { this.labelStatusConfiguracao.Text = messageStatus; });

            for (int inicio = 1; inicio <= progress; inicio++)
            {
                if (this.progressBarStatusConfiguracao.Value < inicio)
                {
                    this.BeginInvoke((MethodInvoker)delegate() { this.progressBarStatusConfiguracao.Value++; });
                    Thread.Sleep(timeSleep);
                }                
            }

            if (progress == 100)
            {
                this.BeginInvoke((MethodInvoker)delegate() { this.progressBarStatusConfiguracao.Value = 0; });
                this.BeginInvoke((MethodInvoker)delegate() { this.panelContainerStatus.Visible = false; });
                this.mThreadStatus.Abort();
            }
        }
    }    
}
