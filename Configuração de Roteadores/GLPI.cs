﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Configuração_de_Roteadores
{
    public class GLPI
    {
        private String TAG = "GLPI";
        private String DIRETORIO = "";
        private String NOME_ARQUIVO = "GLPI.txt";
        private Boolean CABECALHO_ESCRITO = false;
        //private StreamWriter stremWriter;
        
        /*
         * 
         * ************************************* Data da Edicao 10/04/2017 15:11:56 *************************************

     MAC 	  PIN 	       Numero de Serie 	    Modelo
A0AB1B0200C8 	03340004	QX2M2G6075431	roteador_615_n300mb
         */

        public GLPI()
        {
            this.DIRETORIO = System.IO.Path.GetDirectoryName(Application.ExecutablePath);
        }

        public void setDiretorio(String diretorio)
        {
            this.DIRETORIO = diretorio;
        }

        public void setNomeArquvio(String nomeArquivo)
        {
            this.NOME_ARQUIVO = nomeArquivo;
        }

        private string Cabecalho()
        {
            var dataAtual = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            var cabecalho = "************************************* Data da Edicao " + dataAtual + " ************************************* \n\r\n\r";
            cabecalho += "     MAC\t\t   PIN\t\t\tNumero de Serie\t\t\tModelo";

            return cabecalho;
        }

        private string FormatarLinha(string[] dadosLinha)
        {
            var linha = "";
            foreach (string dadoLinha in dadosLinha)
                linha += dadoLinha + "\t\t";
                        
            return linha;
        }

        public void Escrever(string[] dadosLinha)
        {
            var linha = this.FormatarLinha(dadosLinha);
            this.Escrever(linha);
        }

        public void Escrever(String linha)
        {
            StreamWriter streamWriter = null;
            
            if (File.Exists(this.NOME_ARQUIVO))
                streamWriter = File.AppendText(this.DIRETORIO + "\\" + this.NOME_ARQUIVO);
            else 
                streamWriter = new StreamWriter(this.DIRETORIO + "\\" + this.NOME_ARQUIVO);

            try
            {
                if (!this.CABECALHO_ESCRITO)
                {
                    streamWriter.WriteLine(this.Cabecalho());
                    this.CABECALHO_ESCRITO = true;
                }

                linha = linha.Replace("\n", "").Replace("\r", "");
                streamWriter.WriteLine(linha);
                streamWriter.Close();
            }
            catch (NullReferenceException e)
            {
                var mensagemErro = "Não foi possível escrever no Arquivo: " + this.DIRETORIO + "\\" + this.NOME_ARQUIVO + "\n" + e.Message;
                MessageBox.Show(mensagemErro, TAG + " - Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }            
        }
    }
}
